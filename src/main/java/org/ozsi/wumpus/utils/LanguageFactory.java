package org.ozsi.wumpus.utils;

import org.ozsi.wumpus.config.EnglishLanguage;
import org.ozsi.wumpus.config.HungarianLanguage;
import org.ozsi.wumpus.config.Language;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LanguageFactory {

    private static final HungarianLanguage HUNGARIAN_LANGUAGE = new HungarianLanguage();
    private static final EnglishLanguage ENGLISH_LANGUAGE = new EnglishLanguage();

    private static final Map<String, Language> LANGUAGES =
            Stream.of(HUNGARIAN_LANGUAGE, ENGLISH_LANGUAGE)
                    .collect(Collectors.toMap(Language::getLanguageCode, Function.identity()));


    public static Language getLanguage(String code) {
        return LANGUAGES.getOrDefault(code, ENGLISH_LANGUAGE);
    }
}
