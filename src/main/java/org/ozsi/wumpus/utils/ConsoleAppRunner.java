package org.ozsi.wumpus.utils;

import java.util.Scanner;

public class ConsoleAppRunner {

    private final ConsoleController consoleController;

    private ConsoleAppRunner(ConsoleController consoleController) {
        this.consoleController = consoleController;
    }

    public static void run(ConsoleController consoleController) {
        new ConsoleAppRunner(consoleController).start();
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(consoleController.init());
        while (consoleController.isRunning()) {
            System.out.print("> ");
            String line = scanner.nextLine().toLowerCase();
            System.out.println(consoleController.process(line));
        }
    }

}
