package org.ozsi.wumpus.utils;

import org.ozsi.wumpus.config.Language;

public interface ConsoleController {

    String process(String line);
    String init();
    boolean isRunning();

}
