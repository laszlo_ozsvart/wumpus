package org.ozsi.wumpus;

import org.ozsi.wumpus.config.DefaultGameConfig;
import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.GameConfig;
import org.ozsi.wumpus.core.WumpusGame;
import org.ozsi.wumpus.core.WumpusGameImpl;
import org.ozsi.wumpus.entrypoint.console.WumpusConsoleController;
import org.ozsi.wumpus.utils.ConsoleAppRunner;
import org.ozsi.wumpus.utils.ConsoleController;
import org.ozsi.wumpus.utils.LanguageFactory;

public class ConsoleApp {

    public static void main(String[] args) {
        String languageCode = args.length > 0 ? args[0].toLowerCase() : null;
        Language language = LanguageFactory.getLanguage(languageCode);

        WumpusGame wumpusGame = getWumpusGame();
        ConsoleController consoleController = new WumpusConsoleController(wumpusGame, language);
        ConsoleAppRunner.run(consoleController);
    }

    private static WumpusGame getWumpusGame() {
        GameConfig gameConfig = new DefaultGameConfig();
        return new WumpusGameImpl(gameConfig);
    }
}
