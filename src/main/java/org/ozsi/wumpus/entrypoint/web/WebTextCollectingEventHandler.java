package org.ozsi.wumpus.entrypoint.web;

import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.MapTile;
import org.ozsi.wumpus.entrypoint.TextCollectingEventHandler;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class WebTextCollectingEventHandler extends TextCollectingEventHandler {
    private static final String BASE_URL = "http://localhost:8080/";

    public WebTextCollectingEventHandler(Language language) {
        super(language);
    }

    @Override
    public void notifyExits(Set<MapTile> neighbors) {
        super.notifyExits(neighbors);
        List<String> moves = neighbors.stream()
                .map(tile -> BASE_URL + "move/" + tile.getName())
                .collect(Collectors.toList());
        moves.forEach(super::addMessage);
        List<String> shots = neighbors.stream()
                .map(tile -> BASE_URL + "shoot/" + tile.getName())
                .collect(Collectors.toList());
        shots.forEach(super::addMessage);
    }
}
