package org.ozsi.wumpus.entrypoint.web;

import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.WumpusGame;
import org.ozsi.wumpus.entrypoint.TextCollectingEventHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

public class WumpusWebController {

    private final WumpusGame wumpusGame;
    private final Language language;

    public WumpusWebController(WumpusGame wumpusGame, Language language) {
        this.wumpusGame = wumpusGame;
        this.language = language;
    }

    @GetMapping("/start")
    @ResponseBody
    public Object start() {
        TextCollectingEventHandler eventHandler = new WebTextCollectingEventHandler(language);
        wumpusGame.start(eventHandler);
        return eventHandler.getCollected();
    }

    @GetMapping("/move/{target}")
    @ResponseBody
    public Object doMove(@PathVariable("target") String target) {
        TextCollectingEventHandler eventHandler = new WebTextCollectingEventHandler(language);
        wumpusGame.doMove(target, eventHandler);
        return eventHandler.getCollected();
    }

    @GetMapping("/shoot/{target}")
    @ResponseBody
    public Object doShoot(@PathVariable("target") String target) {
        TextCollectingEventHandler eventHandler = new WebTextCollectingEventHandler(language);
        wumpusGame.doShoot(target, eventHandler);
        return eventHandler.getCollected();
    }

}
