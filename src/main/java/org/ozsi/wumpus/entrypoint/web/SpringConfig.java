package org.ozsi.wumpus.entrypoint.web;

import org.ozsi.wumpus.config.DefaultGameConfig;
import org.ozsi.wumpus.config.EnglishLanguage;
import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.WumpusGame;
import org.ozsi.wumpus.core.WumpusGameImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {

    @Bean
    public WumpusGame wumpusGame() {
        return new WumpusGameImpl(
                new DefaultGameConfig()
        );
    }

    @Bean
    public Language defaultLanguage() {
        return new EnglishLanguage();
    }

}
