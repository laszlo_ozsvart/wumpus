package org.ozsi.wumpus.entrypoint.console;

import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.WumpusGame;
import org.ozsi.wumpus.entrypoint.TextCollectingEventHandler;
import org.ozsi.wumpus.utils.ConsoleController;
import org.springframework.stereotype.Controller;

import java.util.stream.Collectors;

@Controller
public class WumpusConsoleController implements ConsoleController {

    private final WumpusGame wumpusGame;
    private final Language language;

    public WumpusConsoleController(WumpusGame wumpusGame, Language language) {
        this.wumpusGame = wumpusGame;
        this.language = language;
    }

    @Override
    public String process(String line) {
        TextCollectingEventHandler eventHandler = new TextCollectingEventHandler(language);
        if (language.isMoveCommand(line)) {
            wumpusGame.doMove(language.getTarget(line), eventHandler);
            return formatEvents(eventHandler);
        } else if (language.isShootCommand(line)) {
            wumpusGame.doShoot(language.getTarget(line), eventHandler);
            return formatEvents(eventHandler);
        } else {
            return language.getInvalidCommandMessage();
        }
    }

    private String formatEvents(TextCollectingEventHandler eventHandler) {
        return eventHandler.getCollected()
                .stream().collect(Collectors.joining("\n"));
    }

    @Override
    public String init() {
        TextCollectingEventHandler eventHandler = new TextCollectingEventHandler(language);
        wumpusGame.start(eventHandler);
        return formatEvents(eventHandler);
    }

    @Override
    public boolean isRunning() {
        return wumpusGame.isRunning();
    }
}
