package org.ozsi.wumpus.entrypoint;

import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.EventHandler;
import org.ozsi.wumpus.core.MapTile;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TextCollectingEventHandler implements EventHandler {

    private final Language language;
    private List<String> collected = new LinkedList<>();

    public TextCollectingEventHandler(Language language) {
        this.language = language;
    }

    @Override
    public void notifyGameStart() {
        collected.add(language.getGameStartMessage());
    }

    @Override
    public void notifyCantMove() {
        collected.add(language.getCantMoveMessage());
    }

    @Override
    public void notifyOutOfArrows() {
        collected.add(language.getOutOfArrowsMessage());
    }

    @Override
    public void notifyArrowsLeft(int arrows) {
        collected.add(language.getNumberOfArrowsMessage(arrows));
    }

    @Override
    public void notifyCantShoot() {
        collected.add(language.getCantShootMessage());
    }

    @Override
    public void notifySmellWumpus() {
        collected.add(language.getSmellWumpusMessage());
    }

    @Override
    public void notifyFeelBreeze() {
        collected.add(language.getFeelBreezeMessage());

    }

    @Override
    public void notifyBatsNearby() {
        collected.add(language.getBatsNearbyMessage());

    }

    @Override
    public void notifyGrabbed() {
        collected.add(language.getGrabbedMessage());
    }

    @Override
    public void notifyGameEnd() {
        collected.add(language.getGameEndMessage());
    }

    @Override
    public void notifyFallen() {
        collected.add(language.getFallenMessage());
    }

    @Override
    public void notifyEaten() {
        collected.add(language.getEatenMessage());
    }

    @Override
    public void notifyMoved(MapTile playerTile) {
        collected.add(language.getArrivalMessage(playerTile.getName()));
    }

    @Override
    public void notifyPosition(MapTile playerTile) {
        collected.add(language.getPositionMessage(playerTile.getName()));
    }

    @Override
    public void notifyExits(Set<MapTile> neighbors) {
        String exits = neighbors.stream().map(MapTile::getName).collect(Collectors.joining(", "));
        collected.add(language.getExitsMessage(exits));
    }

    @Override
    public void notifyWumpusShot() {
        collected.add(language.getWumpusShotMessage());
    }

    @Override
    public void notifyArrowMiss() {
        collected.add(language.getArrowMissMessage());
    }

    @Override
    public void notifyWumpusMove() {
        collected.add(language.getWumpusMoveMessage());

    }

    public void addMessage(String message) {
        collected.add(message);
    }

    public List<String> getCollected() {
        return collected;
    }
}
