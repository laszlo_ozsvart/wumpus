package org.ozsi.wumpus.config;

public class EnglishLanguage implements Language {

    @Override
    public boolean isMoveCommand(String line) {
        return line.startsWith("m ") || line.startsWith("move ");
    }

    @Override
    public boolean isShootCommand(String line) {
        return line.startsWith("s ") || line.startsWith("shoot ");
    }

    @Override
    public String getGameStartMessage() {
        return "***Hunt the Wumpus***\n" +
                "Your tribe has been terrorized by the dreaded Wumpus for years. You've had enough and volunteered to " +
                "finish it once and for all. The Wumpus sleeps in a dangerous cavern of many rooms.\n" +
                "Your job is to track down and shoot the Wumpus without getting eaten or " +
                "falling into a pit. You can move or shoot to neighboring rooms.\n" +
                "Commands: m(ove) 5 or s(hoot) 12";
    }

    @Override
    public String getCantMoveMessage() {
        return "You can't move there!";
    }

    @Override
    public String getOutOfArrowsMessage() {
        return "You have run out of arrows, game over :(";
    }

    @Override
    public String getNumberOfArrowsMessage(int arrows) {
        return "You have " + arrows + " arrows left.";
    }

    @Override
    public String getCantShootMessage() {
        return "You can't shoot there!";
    }

    @Override
    public String getSmellWumpusMessage() {
        return "I smell Wumpus.";
    }

    @Override
    public String getFeelBreezeMessage() {
        return "I feel a breeze.";
    }

    @Override
    public String getBatsNearbyMessage() {
        return "Bats nearby.";
    }

    @Override
    public String getGrabbedMessage() {
        return "Bats grabbed you!";
    }

    @Override
    public String getGameEndMessage() {
        return "";
    }


    @Override
    public String getFallenMessage() {
        return "You fell down, game over :(";
    }

    @Override
    public String getEatenMessage() {
        return "Wumpus got you, you lose :(";
    }

    @Override
    public String getArrivalMessage(String destination) {
        return "You arrive at " + destination;
    }

    @Override
    public String getPositionMessage(String position) {
        return "You are at " + position;
    }

    @Override
    public String getExitsMessage(String exits) {
        return "Possible exits: " + exits;
    }

    @Override
    public String getWumpusShotMessage() {
        return "Congratulations, you win!";
    }

    @Override
    public String getArrowMissMessage() {
        return "You arrow misses.";
    }

    @Override
    public String getWumpusMoveMessage() {
        return "The Wumpus moves...";
    }

    @Override
    public String getInvalidCommandMessage() {
        return "Invalid command, please try again.";
    }

    @Override
    public String getLanguageCode() {
        return "en";
    }

    @Override
    public String getTarget(String line) {
        return line.split(" ")[1];
    }
}
