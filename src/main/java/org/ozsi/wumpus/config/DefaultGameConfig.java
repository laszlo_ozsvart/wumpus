package org.ozsi.wumpus.config;

import org.ozsi.wumpus.core.GameConfig;
import org.ozsi.wumpus.core.GameMap;
import org.ozsi.wumpus.core.GameMapFactory;

public class DefaultGameConfig implements GameConfig {

    @Override
    public int getNumberOfPitTiles() {
        return 2;
    }

    @Override
    public int getNumberOfArrows() {
        return 5;
    }

    @Override
    public int getNumberOfBats() {
        return 2;
    }

    @Override
    public GameMap getGameMap() {
        return GameMapFactory.dodecahedronMap();
    }
}
