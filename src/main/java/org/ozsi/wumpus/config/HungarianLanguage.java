package org.ozsi.wumpus.config;

public class HungarianLanguage implements Language {

    @Override
    public String getLanguageCode() {
        return "hu";
    }

    @Override
    public boolean isMoveCommand(String line) {
        return line.startsWith("m ") || line.startsWith("mozgás ");
    }

    @Override
    public boolean isShootCommand(String line) {
        return line.startsWith("l ") || line.startsWith("lövés ");
    }

    @Override
    public String getGameStartMessage() {
        return "***Wumpusvadászat***\n" +
                "Kis falutokat évek óta rettegésben tartja a kegyetlen Wumpus. Eleged lett ebből és magadra vállaltad " +
                "a feladatot, hogy végezz vele. A Wumpus egy barlangrendszerben alszik.\n" +
                "A feladatod az, hogy megtaláld és levadászd a Wumpus-t anélkül, hogy megenne vagy " +
                "szakadékba esnél. A szomszédos termekbe tudsz mozogni vagy lőni.\n" +
                "Parancsok: m(ozgás) 5 or l(övés) 12";
    }

    @Override
    public String getCantMoveMessage() {
        return "Nem tudsz oda menni";
    }

    @Override
    public String getOutOfArrowsMessage() {
        return "Elfogytak a nyilaid, vége a játéknak :(";
    }

    @Override
    public String getNumberOfArrowsMessage(int arrows) {
        return "Még " + arrows + " nyilad van.";
    }

    @Override
    public String getCantShootMessage() {
        return "Oda nem tudsz lőni!";
    }

    @Override
    public String getSmellWumpusMessage() {
        return "Wumpus szagot érzel.";
    }

    @Override
    public String getFeelBreezeMessage() {
        return "Huzat van.";
    }

    @Override
    public String getBatsNearbyMessage() {
        return "Denevérek a közelben!";
    }

    @Override
    public String getGrabbedMessage() {
        return "Megragadtak a denevérek!";
    }

    @Override
    public String getGameEndMessage() {
        return "";
    }


    @Override
    public String getFallenMessage() {
        return "Szakadékba estél, vége a játéknak :(";
    }

    @Override
    public String getEatenMessage() {
        return "Elkapott a Wumpus, vége :(";
    }

    @Override
    public String getArrivalMessage(String destination) {
        return "A  " + destination + " terembe érkezel.";
    }

    @Override
    public String getPositionMessage(String position) {
        return "A " + position + " teremben vagy.";
    }

    @Override
    public String getExitsMessage(String exits) {
        return "Kijáratok: " + exits;
    }

    @Override
    public String getWumpusShotMessage() {
        return "Gratulálok! Legyőzted a Wumpus-t!";
    }

    @Override
    public String getArrowMissMessage() {
        return "A nyilad nem talált el semmit.";
    }

    @Override
    public String getWumpusMoveMessage() {
        return "A Wumpus odébb mászik...";
    }

    @Override
    public String getInvalidCommandMessage() {
        return "Helytelen paracs, próbálkozz újra.";
    }

    @Override
    public String getTarget(String line) {
        return line.split(" ")[1];
    }
}
