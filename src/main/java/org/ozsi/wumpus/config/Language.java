package org.ozsi.wumpus.config;

public interface Language {
    String getLanguageCode();

    boolean isMoveCommand(String line);

    boolean isShootCommand(String line);

    String getGameStartMessage();

    String getCantMoveMessage();

    String getOutOfArrowsMessage();

    String getNumberOfArrowsMessage(int arrows);

    String getCantShootMessage();

    String getSmellWumpusMessage();

    String getFeelBreezeMessage();

    String getBatsNearbyMessage();

    String getGrabbedMessage();

    String getGameEndMessage();

    String getFallenMessage();

    String getEatenMessage();

    String getArrivalMessage(String destination);

    String getPositionMessage(String position);

    String getExitsMessage(String exits);

    String getWumpusShotMessage();

    String getArrowMissMessage();

    String getWumpusMoveMessage();

    String getInvalidCommandMessage();

    String getTarget(String line);
}
