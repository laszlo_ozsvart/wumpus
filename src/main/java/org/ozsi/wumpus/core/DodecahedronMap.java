package org.ozsi.wumpus.core;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class DodecahedronMap implements GameMap {

    private static final int[][] OTHER_EDGES = {
            {7, 11},
            {5, 12},
            {6, 19},
            {8, 18},
            {1, 17},
            {0, 4},
            {2, 15},
            {3, 13},
            {9, 16},
            {10, 14}};

    private final Map<MapTile, Set<MapTile>> neighbors;
    private static final Map<Integer, MapTile> TILES_BY_ID = IntStream.range(0, 20)
            .boxed()
            .collect(Collectors.toMap(Function.identity(), IntegerMapTile::new));
    private static final Map<String, MapTile> TILE_BY_NAME = TILES_BY_ID.values().stream()
            .collect(Collectors.toMap(MapTile::getName, Function.identity()));
    private final Random random;

    public DodecahedronMap(Random random) {
        this.random = random;
        neighbors = IntStream.range(0, 20)
                .boxed()
                .collect(Collectors.toMap(TILES_BY_ID::get, i-> setOf((i + 19) % 20, (i + 1) % 20, getPair(i), TILES_BY_ID)));
    }

    private Set<MapTile> setOf(int a, int b, int c, Map<Integer, MapTile> tilesById) {
        return Stream.of(a, b, c)
                .map(tilesById::get)
                .collect(Collectors.toSet());
    }

    private int getPair(int tileId) {
        for (int[] edge: OTHER_EDGES) {
            if (edge[0] == tileId) return edge[1];
            if (edge[1] == tileId) return edge[0];
        }
        return -1;
    }

    @Override
    public Set<MapTile> getTiles() {
        return neighbors.keySet();
    }

    @Override
    public boolean isIncidental(MapTile tile1, MapTile tile2) {
        return neighbors.get(tile1).contains(tile2);
    }

    @Override
    public Set<MapTile> getNeighbors(MapTile tile) {
        return neighbors.get(tile);
    }

    @Override
    public MapTile getTileByName(String name) {
        return TILE_BY_NAME.get(name);
    }

    @Override
    public MapTile getRandomNeighborOrTile(MapTile tile) {
        List<MapTile> neighborsAndTile = Stream.concat(Stream.of(tile), this.neighbors.get(tile).stream())
                .collect(Collectors.toList());
        return neighborsAndTile.get(random.nextInt(neighborsAndTile.size()));

    }

    @Override
    public MapTile getRandomTile() {
        return TILES_BY_ID.get(random.nextInt(TILES_BY_ID.size()));
    }

    @Override
    public List<MapTile> getRandomTiles(int number) {
        Map<MapTile, Integer> randomValues = TILES_BY_ID.values().stream()
                .collect(Collectors.toMap(Function.identity(), tile -> random.nextInt()));

        return randomValues.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .limit(number)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
