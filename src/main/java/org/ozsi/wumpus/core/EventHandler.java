package org.ozsi.wumpus.core;

import java.util.Set;

public interface EventHandler {

    void notifyGameStart();

    void notifyCantMove();

    void notifyOutOfArrows();

    void notifyArrowsLeft(int arrows);

    void notifyCantShoot();

    void notifySmellWumpus();

    void notifyFeelBreeze();

    void notifyBatsNearby();

    void notifyGrabbed();

    void notifyGameEnd();

    void notifyFallen();

    void notifyEaten();

    void notifyMoved(MapTile playerTile);

    void notifyPosition(MapTile playerTile);

    void notifyExits(Set<MapTile> neighbors);

    void notifyWumpusShot();

    void notifyArrowMiss();

    void notifyWumpusMove();
}
