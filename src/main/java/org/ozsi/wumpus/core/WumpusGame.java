package org.ozsi.wumpus.core;

public interface WumpusGame {

    void start(EventHandler eventhandler);
    void doMove(String targetTileName, EventHandler eventHandler);
    void doShoot(String targetTileName, EventHandler eventHandler);
    boolean isRunning();

}
