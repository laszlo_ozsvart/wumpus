package org.ozsi.wumpus.core;

import java.util.List;
import java.util.Set;

public interface GameMap {

    Set<MapTile> getTiles();
    boolean isIncidental(MapTile tile1, MapTile tile2);
    Set<MapTile> getNeighbors(MapTile tile);
    MapTile getRandomTile();
    MapTile getRandomNeighborOrTile(MapTile tile);
    MapTile getTileByName(String name);
    List<MapTile> getRandomTiles(int number);

}
