package org.ozsi.wumpus.core;

public interface GameConfig {
    int getNumberOfPitTiles();
    int getNumberOfArrows();
    int getNumberOfBats();
    GameMap getGameMap();
}
