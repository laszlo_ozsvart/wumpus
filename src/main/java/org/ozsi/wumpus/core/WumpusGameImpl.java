package org.ozsi.wumpus.core;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class WumpusGameImpl implements WumpusGame {

    private final GameMap gameMap;

    private MapTile playerTile;
    private MapTile wumpusTile;
    private Set<MapTile> batTiles;
    private Set<MapTile> pitTiles;
    private int arrows;
    private boolean active;

    public WumpusGameImpl(GameConfig gameConfig) {
        this.gameMap = gameConfig.getGameMap();
        init(gameConfig);
    }

    @Override
    public void start(EventHandler eventhandler) {
        eventhandler.notifyGameStart();
        doStatusNotification(false, eventhandler);
        doEnvironmentNotifications(eventhandler);
    }

    @Override
    public void doMove(String targetTileName, EventHandler eventHandler) {
        if (!active) {return;}
        MapTile target = gameMap.getTileByName(targetTileName);
        if (target != null && gameMap.isIncidental(playerTile, target)) {
            playerTile = target;
            interactWithTile(eventHandler);
            if (active) {
                doStatusNotification(true, eventHandler);
                doEnvironmentNotifications(eventHandler);
            }
        } else {
            eventHandler.notifyCantMove();
        }
    }

    @Override
    public void doShoot(String targetTileName, EventHandler eventHandler) {
        if (!active) {return;}
        MapTile target = gameMap.getTileByName(targetTileName);
        if (target != null && gameMap.isIncidental(playerTile, target)) {
            if (target == wumpusTile) {
                eventHandler.notifyWumpusShot();
                eventHandler.notifyGameEnd();
                active = false;
            } else {
                eventHandler.notifyArrowMiss();
                eventHandler.notifyWumpusMove();
                moveWumpus(gameMap.getRandomNeighborOrTile(wumpusTile), eventHandler);
                if (active) {
                    doEnvironmentNotifications(eventHandler);
                    if (arrows <= 0) {
                        eventHandler.notifyOutOfArrows();
                        active = false;
                    } else {
                        eventHandler.notifyArrowsLeft(arrows);
                    }
                }
            }
            arrows--;
        } else {
            eventHandler.notifyCantShoot();
        }
    }

    private void moveWumpus(MapTile target, EventHandler eventHandler) {
        wumpusTile = target;
        checkWumpus(eventHandler);
    }

    private void doEnvironmentNotifications(EventHandler eventHandler) {
        if (gameMap.isIncidental(playerTile, wumpusTile)) {
            eventHandler.notifySmellWumpus();
        }
        if (pitTiles.stream().anyMatch(pitTile -> gameMap.isIncidental(pitTile, playerTile))) {
            eventHandler.notifyFeelBreeze();
        }
        if (batTiles.stream().anyMatch(batTile -> gameMap.isIncidental(batTile, playerTile))) {
            eventHandler.notifyBatsNearby();
        }
    }

    private void interactWithTile(EventHandler eventHandler) {
        checkWumpus(eventHandler);
        checkPit(eventHandler);
        checkBats(eventHandler);
    }

    private void checkBats(EventHandler eventHandler) {
        if (batTiles.contains(playerTile)) {
            MapTile destination = gameMap.getRandomTile();
            eventHandler.notifyGrabbed();
            playerTile = destination;
            interactWithTile(eventHandler);
        }
    }

    private void checkPit(EventHandler eventHandler) {
        if (pitTiles.contains(playerTile)) {
            eventHandler.notifyFallen();
            eventHandler.notifyGameEnd();
            active = false;
        }
    }

    private void checkWumpus(EventHandler eventHandler) {
        if (playerTile == wumpusTile) {
            eventHandler.notifyEaten();
            eventHandler.notifyGameEnd();
            active = false;
        }
    }

    private void doStatusNotification(boolean moved, EventHandler eventHandler) {
        if (moved) {
            eventHandler.notifyMoved(playerTile);
        } else {
            eventHandler.notifyPosition(playerTile);
        }
        eventHandler.notifyExits(gameMap.getNeighbors(playerTile));
        eventHandler.notifyArrowsLeft(arrows);
    }

    private void init(GameConfig gameConfig) {
        List<MapTile> tiles = gameMap.getRandomTiles(
                2 +
                gameConfig.getNumberOfPitTiles() +
                gameConfig.getNumberOfBats());
        playerTile = tiles.get(0);
        wumpusTile = tiles.get(1);
        pitTiles = tiles.stream()
                .skip(2)
                .limit(gameConfig.getNumberOfPitTiles())
                .collect(Collectors.toSet());
        batTiles = tiles.stream()
                .skip(2 + gameConfig.getNumberOfPitTiles())
                .limit(gameConfig.getNumberOfBats())
                .collect(Collectors.toSet());
        arrows = gameConfig.getNumberOfArrows();
        active = true;
    }

    @Override
    public boolean isRunning() {
        return active;
    }
}
