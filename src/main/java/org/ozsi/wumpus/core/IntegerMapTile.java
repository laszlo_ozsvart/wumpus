package org.ozsi.wumpus.core;

class IntegerMapTile implements MapTile {

    private final int tile;

    public IntegerMapTile(int tile) {
        this.tile = tile;
    }

    @Override
    public String getName() {
        return tile + "";
    }

}
