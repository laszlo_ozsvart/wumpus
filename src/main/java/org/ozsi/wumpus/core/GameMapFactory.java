package org.ozsi.wumpus.core;

import java.util.Random;

public class GameMapFactory {

    public static GameMap dodecahedronMap(int seed) {
        return new DodecahedronMap(new Random(seed));
    }

    public static GameMap dodecahedronMap() {
        return dodecahedronMap(new Random().nextInt());
    }
}
