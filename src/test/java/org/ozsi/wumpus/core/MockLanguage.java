package org.ozsi.wumpus.core;

import org.ozsi.wumpus.config.Language;

public class MockLanguage implements Language {
    @Override
    public String getLanguageCode() {
        return "mock";
    }

    @Override
    public boolean isMoveCommand(String line) {
        return line.startsWith("m ");
    }

    @Override
    public boolean isShootCommand(String line) {
        return line.startsWith("s ");
    }

    @Override
    public String getGameStartMessage() {
        return "start";
    }

    @Override
    public String getCantMoveMessage() {
        return "cantmove";
    }

    @Override
    public String getOutOfArrowsMessage() {
        return "out of arrows";
    }

    @Override
    public String getNumberOfArrowsMessage(int arrows) {
        return arrows + " arrows";
    }

    @Override
    public String getCantShootMessage() {
        return "can't shoot";
    }

    @Override
    public String getSmellWumpusMessage() {
        return "smell wumpus";
    }

    @Override
    public String getFeelBreezeMessage() {
        return "feel breeze";
    }

    @Override
    public String getBatsNearbyMessage() {
        return "bats nearby";
    }

    @Override
    public String getGrabbedMessage() {
        return "grabbed";
    }

    @Override
    public String getGameEndMessage() {
        return "end of game";
    }

    @Override
    public String getFallenMessage() {
        return "fallen";
    }

    @Override
    public String getEatenMessage() {
        return "eaten";
    }

    @Override
    public String getArrivalMessage(String destination) {
        return "arrived at " + destination;
    }

    @Override
    public String getPositionMessage(String position) {
        return "at " + position;
    }

    @Override
    public String getExitsMessage(String exits) {
        return "exits at " + exits;
    }

    @Override
    public String getWumpusShotMessage() {
        return "wumpus shot";
    }

    @Override
    public String getArrowMissMessage() {
        return "arrow miss";
    }

    @Override
    public String getWumpusMoveMessage() {
        return "wumpus moves";
    }

    @Override
    public String getInvalidCommandMessage() {
        return "Invalid command";
    }

    @Override
    public String getTarget(String line) {
        return line;
    }
}
