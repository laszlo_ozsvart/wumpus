package org.ozsi.wumpus.core;

public class NoHazardsGameConfig implements GameConfig {

    @Override
    public int getNumberOfPitTiles() {
        return 0;
    }

    @Override
    public int getNumberOfArrows() {
        return 1;
    }

    @Override
    public int getNumberOfBats() {
        return 0;
    }

    @Override
    public GameMap getGameMap() {
        return new MockGameMap();
    }
}
