package org.ozsi.wumpus.core;

public class MockMapTile implements MapTile {

    private final String name;

    public MockMapTile(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
