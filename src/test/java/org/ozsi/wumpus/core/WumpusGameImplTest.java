package org.ozsi.wumpus.core;

import org.junit.Test;
import org.mockito.Mockito;

public class WumpusGameImplTest {


    @Test
    public void shouldSmellWumpusOnStart() {
        WumpusGame wumpusGame = new WumpusGameImpl(
                new NoHazardsGameConfig()
        );

        EventHandler eventHandler = Mockito.mock(EventHandler.class);
        wumpusGame.start(eventHandler);

        Mockito.verify(eventHandler).notifySmellWumpus();
    }

    @Test
    public void shouldNotifyArrowsOnStart() {
        WumpusGame wumpusGame = new WumpusGameImpl(
                new NoHazardsGameConfig() {
                    @Override
                    public int getNumberOfArrows() {
                        return 2;
                    }
                }
        );

        EventHandler eventHandler = Mockito.mock(EventHandler.class);
        wumpusGame.start(eventHandler);

        Mockito.verify(eventHandler).notifyArrowsLeft(2);
    }


}