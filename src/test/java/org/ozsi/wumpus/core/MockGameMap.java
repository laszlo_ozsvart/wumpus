package org.ozsi.wumpus.core;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MockGameMap implements GameMap {
    private static Set<MapTile> TILES = Stream.of("a", "b", "c")
            .map(MockMapTile::new)
            .collect(Collectors.toSet());


    @Override
    public Set<MapTile> getTiles() {
        return TILES;
    }

    @Override
    public boolean isIncidental(MapTile tile1, MapTile tile2) {
        return tile1 != tile2;
    }

    @Override
    public Set<MapTile> getNeighbors(MapTile tile) {
        return TILES.stream()
                .filter(otherTile -> !otherTile.getName().equals(tile.getName()))
                .collect(Collectors.toSet());
    }

    @Override
    public MapTile getRandomTile() {
        return TILES.iterator().next();
    }

    @Override
    public MapTile getRandomNeighborOrTile(MapTile tile) {
        return tile;
    }

    @Override
    public MapTile getTileByName(String name) {
        return TILES.stream()
                .filter(tile -> name.equals(tile.getName()))
                .findFirst().get();
    }

    @Override
    public List<MapTile> getRandomTiles(int number) {
        return TILES.stream()
                .limit(number)
                .collect(Collectors.toList());
    }
}
