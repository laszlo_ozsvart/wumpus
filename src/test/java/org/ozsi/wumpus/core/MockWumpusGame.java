package org.ozsi.wumpus.core;

public class MockWumpusGame implements WumpusGame {

    @Override
    public void start(EventHandler eventhandler) {
    }

    @Override
    public void doMove(String targetTileName, EventHandler eventHandler) {
    }

    @Override
    public void doShoot(String targetTileName, EventHandler eventHandler) {
    }

    @Override
    public boolean isRunning() {
        return false;
    }
}
