package org.ozsi.wumpus.entrypoint.console;

import org.hamcrest.core.Is;
import org.junit.Assert;
import org.junit.Test;
import org.ozsi.wumpus.config.Language;
import org.ozsi.wumpus.core.EventHandler;
import org.ozsi.wumpus.core.MockLanguage;
import org.ozsi.wumpus.core.MockWumpusGame;
import org.ozsi.wumpus.core.WumpusGame;

import static org.hamcrest.core.Is.is;

public class WumpusConsoleControllerTest {

    @Test
    public void initShouldStartGame() {
        WumpusGame wumpusGame = new MockWumpusGame() {
            @Override
            public void start(EventHandler eventhandler) {
                eventhandler.notifyGameStart();
            }
        };
        Language language = new MockLanguage() {
            @Override
            public String getGameStartMessage() {
                return "start";
            }
        };
        WumpusConsoleController controller = new WumpusConsoleController(wumpusGame, language);

        String actual = controller.init();

        String expected = "start";
        Assert.assertThat(actual, is(expected));
    }

    @Test
    public void processShouldHandleMovement() {
        WumpusGame wumpusGame = new MockWumpusGame() {
            @Override
            public void doMove(String targetTileName, EventHandler eventHandler) {
                eventHandler.notifyFeelBreeze();
                eventHandler.notifyBatsNearby();
            }
        };
        Language language = new MockLanguage() {
            @Override
            public String getFeelBreezeMessage() {
                return "breeze";
            }

            @Override
            public String getBatsNearbyMessage() {
                return "bats";
            }
        };
        WumpusConsoleController controller = new WumpusConsoleController(wumpusGame, language);


        String actual = controller.process("m 1");

        Assert.assertThat(actual, Is.is("breeze\nbats"));
    }

    @Test
    public void processShouldHandleShooting() {
        WumpusGame wumpusGame = new MockWumpusGame() {
            @Override
            public void doShoot(String targetTileName, EventHandler eventHandler) {
                eventHandler.notifyArrowMiss();
            }
        };
        Language language = new MockLanguage() {
            @Override
            public String getArrowMissMessage() {
                return "missed";
            }
        };
        WumpusConsoleController controller = new WumpusConsoleController(wumpusGame, language);

        String actual = controller.process("s 1");

        Assert.assertThat(actual, Is.is("missed"));
    }

    @Test
    public void processShouldHandleInvalidCommand() {
        WumpusGame wumpusGame = new MockWumpusGame() ;
        Language language = new MockLanguage() {
            @Override
            public String getInvalidCommandMessage() {
                return "Invalid command";
            }
        };
        WumpusConsoleController controller = new WumpusConsoleController(wumpusGame, language);

        String actual = controller.process("move1");
        Assert.assertThat(actual, is("Invalid command"));
    }

}